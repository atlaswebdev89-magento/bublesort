<?php

namespace Atlas89\Sort;

class BubleSort
{
    public static function bubleSort(array $data = null)
    {
        if (isset($data) && is_array($data)) {
            $result = self::getSortArray($data);
            dump($result);
        } else {
            throw new \Exception("Not array parametr");
        }
    }

    private static function getSortArray(array $array)
    {
        for ($i = 0; $i < count($array) - 1; $i++) {
            for ($j = 0; $j < count($array) - 1 - $i; $j++) {
                $elem = $array[$j];
                $next = $array[$j + 1];
                if ($elem > $next) {
                    $array[$j + 1] = $elem;
                    $array[$j]     = $next;
                }
            }
        }

        return $array;
    }
}